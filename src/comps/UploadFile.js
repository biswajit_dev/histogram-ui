import React, { useState } from "react";
import { fileTypeError } from "../utils/enum";
import ProgressBar from "./ProgressBar";

const UploadFile = () => {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);

  const allowFileType = ["image/jpeg", "image/png", "image/jpg"]

  const handelChange = event => {
    let selectedFile = event.target.files[0];
    if(selectedFile && allowFileType.includes(selectedFile.type)){
      setFile(selectedFile);
      setError(null)
    }else{
      setFile(null);
      setError(fileTypeError.INVALID_FILE)
    }
  }

  return(
    <div>
      <label>
        <input type="file" onChange={handelChange} />
        <span>+</span>
      </label>
      <div className="output">
        {
          error && <div className="error">{ error }</div>
        }
        {
          file && <ProgressBar file={file} setFile={setFile}/>
        }
      </div>
    </div>
  )
}

export default UploadFile;
