import React from 'react'
import UseFirestore from '../hooks/useFirestore';
import { motion } from "framer-motion";

const ImageGrid = ({ setSelectedImg }) => {
    const { docs } = UseFirestore('images');

    const handelClick = url => {
        setSelectedImg(url)
    }

    return (
        <div className="img-grid">
            {
                docs && (
                    docs.map(doc => (
                        <motion.div 
                            layout
                            whileHover={{opacity: 1}}
                            className="img-wrap" 
                            key={doc.id} 
                            onClick={()=> handelClick(doc.url)}
                        >
                            <motion.img src={doc.url} alt="uploaded-image" 
                                initial={{ opacity: 0 }}
                                animate={{ opacity:1 }}
                                transition={{ delay: 1 }}
                            />
                        </motion.div>
                    ))
                )
            }
        </div>
    )
}

export default ImageGrid;