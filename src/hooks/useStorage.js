import { useState, useEffect } from "react";
import { projectStorage, projectFirestore, timeStamp } from "../firebase/config";

const UseStorage = file => {
    const [progress, setProgress] = useState(null);
    const [url, setUrl] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        const storageRef = projectStorage.ref(file.name);
        const collectionRef = projectFirestore.collection('images');

         storageRef.put(file).on('state_changed', (snap) => {
            const uploadedPercentage = (snap.bytesTransferred/snap.totalBytes) * 100;
            setProgress(uploadedPercentage);
        },(err) =>{
            setError(err);
        }, async() =>{
            const url = await storageRef.getDownloadURL();
            const createdAt = timeStamp();
            collectionRef.add({ url, createdAt })
            setUrl(url);
        })
    }, [file]);

    return {
        progress,
        error,
        url
    }
}

export default UseStorage;