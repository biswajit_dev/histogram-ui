import * as firebase from "firebase/app";
import "firebase/storage";
import "firebase/firestore";

let firebaseConfig = {
    apiKey: "AIzaSyAMEz3-dtmy4mocdBU10GOWnTBwuV9AGgs",
    authDomain: "histogram-7a380.firebaseapp.com",
    databaseURL: "https://histogram-7a380.firebaseio.com",
    projectId: "histogram-7a380",
    storageBucket: "histogram-7a380.appspot.com",
    messagingSenderId: "318111059677",
    appId: "1:318111059677:web:d378934c293bd569f79534"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const projectStorage = firebase.storage();
  const projectFirestore = firebase.firestore();
  const timeStamp = firebase.firestore.FieldValue.serverTimestamp;

  export { projectStorage, projectFirestore, timeStamp };
